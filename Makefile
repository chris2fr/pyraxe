webfaction:
	source ./bin/activate
	easy_install -U Editor
	deactivate
	./bin/restart

mannbookpro:
	../bin/paster serve mannbookpro.ini

test:
	../bin/python editor/resources.py -v
