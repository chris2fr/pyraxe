# coding=utf-8
#
# file for business logic and root for transversal
#
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import os # For saving, opening, files
from pyramid.view import view_config
from xml.dom.minidom import *
import glob
from pyramid.security import Allow
#from pyramid.security import Everyone
from pyramid.security import Authenticated
from pyramid.security import ALL_PERMISSIONS
import filecmp

class Editor(object):
    """Handles logic.

    :param data_path: Relative location of files to executed file. (../data/)"""

    __acl__ = [
        (Allow, Authenticated, 'edit'),
        (Allow, 'editors', 'edit'),
        (Allow, 'admins', ALL_PERMISSIONS),
    ]
    ##ici, probleme
    def __init__(self, data_path = "../data/", request = None):
        """Constructs.

        :param data_path: Value for self.data_path
        """

        self.data_path = data_path
        self.request = request

    def _get_data_path(self): #get the data folder's path
        os.chdir(self.data_path)
        path = os.getcwd()
        return path

    def _get_path(self, basename):
        path = self._get_data_path()
        path = os.path.join(self._get_data_path(),basename)
        return path

    def insert(self,
               basename,
               xhtmlBody,
               xhtml_title = "TITLE",
               layout = "email"):
        """Makes a new XHTML File as basename.xhtml ans with xhtmlBody in the body tag.
        >>> xd = Editor().insert('test.xhtml','test insert in body')
        >>> xd.toDict()['basename']
        'test.xhtml'
        >>> xd.toDict()['xhtmlBody']
        'test insert in body'
        >>> xd = Editor().insert('test.xhtml','Test body, this is a test.')
        >>> xd.toDict()['basename']
        'test.xhtml'
        """
        xd = XHTMLDoc(basename = basename,
                      title = xhtml_title,
                      layout = layout,
                      body = xhtmlBody,
                      data_dir_path = self.data_path)
        xd.save_and_version()
        return xd

    def select(self, basename, version = None):
        """Selects a file and returns the BODY part.
        >>> Editor().insert('test.xhtml','test insert ≥ in body.').toDict()['basename']
        'test.xhtml'
        >>> Editor().select('test.xhtml').toDict()['xhtmlBody'].decode("utf8")
        u'test insert \u2265 in body.'
        """
        #lire un ficher, et recuper contenu entre les balises <body></body>
        xdoc = XHTMLDoc(basename = basename,
                        data_dir_path = self.data_path,
                        version = version)
        xdoc.load()
        return xdoc

    def update(self, basename, xhtmlBody, xhtml_title = "TITLE",
               layout = "email", rebasename = None):
        """Inserts xhtmlBody in the Body part.
        >>> Editor().update('test.xhtml','test update é in body').toDict()['basename']
        'test.xhtml'
        >>> Editor().select('test.xhtml').toDict()['xhtmlBody']
        u'test update \\xe9 in body'
        >>> Editor().update('test.xhtml','Test body, this is a test.').toDict()['basename']
        'test.xhtml'
        """
        if rebasename and not rebasename == basename:
            xdoc = self.select(basename)
            xdoc.rename(rebasename)
            basename = rebasename
        return self.insert(basename, xhtmlBody, xhtml_title = xhtml_title, layout = layout)

    def delete(self, basename):
        """Deletes the basename.xhtml file."""
        xd = XHTMLDoc(basename = basename,
              data_dir_path = self.data_path)
        xd.remove()
        return xd

    def create(self, basename):
        """Prepares the insert of basename.xhtml."""
        xdoc = XHTMLDoc(basename = basename,
                        data_dir_path = self.data_path )
        return xdoc

    def edit(self, basename):
        """Prepares the edition of a xhtmlFile."""
        return self.select(basename)

    def list(self):
        """Lists available files."""
        # Tuple from os.Dir isting of the files in data
        self._get_data_path()
        fileList = glob.glob('*.html')
        fileList.sort()
        return fileList

    def home(self):
        """Prepares the index/home/root page."""
        return self.list()

    def restore(self, basename):
        """Restores archived file.

        :param basename: The identity of the file to restore."""
        xd = XHTMLDoc(basename = basename,
              data_dir_path = self.data_path)
        xd.restore()
        xd.load()
        return xd


class XHTMLDoc (object):
    """Business Object representing XHTML Files.

    :param body: XHTML content to be in the body
    :param title: Page title
    :param basename: A a name to use in the filesystem without spaces
    :param layout: A name of the layout to apply
    :param data_dir_path: The full path to the file
    :param version: Int, the current version of this file
    :param max_versions: The maximum number of versions to save
    :param _versions_dir_path: The diretory where the versions and archives are stored
    """

    _versions_dir_path = None

    def __init__(self,
                body = None,
                title = None,
                basename = None,
                layout = None,
                data_dir_path = None,
                version = None,
                max_versions = 30):
        """Constructs document object.

        :param body: XHTML content to be in the body
        :param title: Page title
        :param basename: A a name to use in the filesystem without spaces
        :param layout: A name of the layout to apply
        :param data_dir_path: The full path to the where the files are
        :param version: Int, the current version of this file
        :param max_versions: The maximum number of versions to save
        """
        self.body = body
        self.title = title
        self.basename = basename
        self.layout = layout
        self.data_dir_path = data_dir_path
        self.version = version
        self.max_versions = max_versions

    def _get_versions_dir_path(self):
        """Returns the full directory path of where the version files are stored."""
        if not self._versions_dir_path:
            if not os.path.isdir(os.path.join(self.data_dir_path,"_versions")):
                os.mkdir(os.path.join(self.data_dir_path,"_versions"))
            self._versions_dir_path = os.path.join(self.data_dir_path,"_versions")
        return os.path.join(self._versions_dir_path)

    def get_path(self, version = None):
        """Returns the full path : data_dir_path and basename."""
        if version != None:
            return self.version_path(version = version)
        return os.path.join(self.data_dir_path,self.basename)

    def toDict(self):
        """Returns a dictionary of useful attributes.
        """
        return {'xhtmlBody':self.body,
                'xhtmlTitle':self.title,
                'basename':self.basename,
                'layout':self.layout,
                'data_dir_path':self.data_dir_path,
                'path':self.get_path(version = self.version),
                'version':self.version
                }

    def rename (self, basename):
        """Changes the filename.

        :param basename: the new filename."""
        oldbasename = self.basename
        os.rename(os.path.join(self.data_dir_path,oldbasename), os.path.join(self.data_dir_path,basename))
        for i in range(0,self.max_versions):
            if os.path.isfile(self.version_path(basename=oldbasename, version=i)):
                os.rename(self.version_path(basename=oldbasename, version=i), self.version_path(basename=basename, version=i))

    def version_number(self):
        """Returns the current version number as self-reported, or from the filesystem."""
        if self.version != None:
            if int(self.version) < 0:
                self.version = 0
            return self.version
        i = 0
        while os.path.isfile(self.version_path(version=i)) and i <= self.max_versions:
            i += 1
        self.version = (i - 1 % self.max_versions)
        return self.version

    def version_path(self, version = None, basename = None):
        """Returns the full path of a version of a file.

        :param basename: an alternate file
        :param version: an alternate version"""
        path = self._get_versions_dir_path()
        if version == None:
            version =  self.version_number()
        elif version == -1:
            version =  self.version_number() - 1
        if basename == None:
            basename =  self.basename
        return os.path.join(self._get_versions_dir_path(), basename + ".%02i" % (version % self.max_versions))

    def save_and_version(self):
        """Saves the file, and stores a new version."""
        self.save()
        if os.path.isfile(self.version_path()) and filecmp.cmp(self.version_path(), self.get_path()):
            return
        elif os.path.isfile(self.version_path()): # Initial case
            self.version += 1
        else:
            self.version = 0
        self.save()
        os.rename(self.get_path(), self.version_path())
        self.save()
        nextpath = self.version_path(self.version + 1)
        if os.path.isfile(nextpath):
            os.remove(nextpath)

    def remove(self):
        """Archives the file"""
        self.load()
        self.save_and_version()
        os.rename(self.get_path(),os.path.join(self._get_versions_dir_path(),self.basename))
        #os.remove(self.get_path())

    def restore(self):
        """Restores an archived file."""
        os.rename(os.path.join(self._get_versions_dir_path(),self.basename),self.get_path())

    def load(self):
        """Opens and reads an HTML file into memory.

        :param version: optional past version to load"""
        path = self.get_path(version = self.version)
        odom = parse(path)
        self.body = odom.getElementsByTagName('body').item(0).toxml()[7:-8]
        self.title = odom.getElementsByTagName('title').item(0).toxml()[7:-8]
        mts = odom.getElementsByTagName('meta')
        for mt in mts:
            if mt.hasAttribute("name"):
                if mt.getAttribute("name") == "layout":
                    self.layout = mt.getAttribute("value")
                if mt.getAttribute("name") == "version":
                    self.version = int(mt.getAttribute("value"))

    def save(self):
        """Saves the current file to disk in HTML form."""
        f = file('%s' % self.get_path(),'w')#the saved file's name is in the path?
        try:
            head =u"""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>%s</title>
    <meta name="layout" value="%s"/>
</head>
<body>
"""

            head =u"""<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>%s</title>
    <meta name="layout" value="%s"/>
    <meta name="version" value="%s"/>
</head>
<body>
"""
            tail = u"""
</body>
</html>"""
            head = head % (self.title, self.layout, self.version_number() )
            f.write(head)
            f.write(self.body)
            f.write(tail)
        finally:
            f.close #write the content to the created file

def test_select(basename):
    editor=Editor()
    dict=editor.select(basename)
    return dict['xhtmlBody']

def test_list():
    editor = Editor()
    editor.list()

def test_insert(basename,xhtmlBody):
    editor = Editor()
    editor.insert(basename,xhtmlBody)

def main():
##    print test_select('test.xhtml')
##    test_list()
##    import doctest, resources
##    doctest.testmod(resources)
    import doctest, resources
    doctest.testmod(resources)
    test_insert('test','test')
    pass

if __name__ == '__main__':
    main()


