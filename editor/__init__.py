from pyramid.config import Configurator
from editor.resources import Editor
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
# Get our database that manages users
from usersdb import groupfinder


def hello_world(request):
   return Response('Hello %(name)s!' % request.matchdict)

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    authz_policy = ACLAuthorizationPolicy()
    config = Configurator(root_factory=Editor, settings=settings,
                            authentication_policy=AuthTktAuthenticationPolicy('seekr1tuufp',
                            callback=groupfinder),
                            authorization_policy=authz_policy)

    config.add_route('list',pattern='/list')
    config.add_route('create',pattern='/create')
    config.add_route('edit',pattern='/edit/{basename}')
    config.add_route('select',pattern='/select/{basename}')
    config.add_route('insert',pattern='/insert/{basename}')
    config.add_route('update',pattern='/update/{basename}')
    config.add_route('delete',pattern='/delete/{basename}')
    config.add_route('view_raw',pattern='/raw/{basename}')
    config.add_route('download',pattern='/download/{basename}')
    config.add_route('view_email',pattern='/email/{basename}')
    config.add_route('view_web',pattern='/web/{basename}')
    config.add_route('view_news',pattern='/news/{basename}')
    config.add_route('restore',pattern='/restore')
    config.add_route('home','/')
    #vues pour les apps mobile
    config.add_route('filelist','/filelist')
    #pour le contenu du fichier
    config.add_route(name='filecontent',pattern='/file/{basename}')

    #config.add_view(view=v_home, route_name='home',
    #                renderer='editor:templates/home.pt')
    #config.add_view('editor.views.my_view',
    #                context='editor:resources.Root',
    #                renderer='editor:templates/home.pt')

    config.add_static_view('static', 'editor:static', cache_max_age=3600)
    config.add_static_view('html', 'editor:html', cache_max_age=5)
    config.scan('editor')
    #config.include('pyramid_debugtoolbar')
    return config.make_wsgi_app()