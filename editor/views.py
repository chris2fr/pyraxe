# coding=utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from pyramid.view import view_config
from editor.resources import Editor
import os
from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPForbidden
from pyramid.security import remember
from pyramid.security import forget
import simplejson
#from pyramid.security import Allow
#from pyramid.security import Everyone
from pyramid.response import Response

# Get our database that manages users
from usersdb import USERS
from pyramid.security import has_permission


def my_view(request):
    return {'project':'Editor'}


@view_config(renderer="templates/login.pt", context=HTTPForbidden)
#@view_config(renderer="templates/login.pt", name="login.html")
def login(request):
    message = ''
    login = ''
    password = ''
    if 'form.submitted' in request.params:
        login = request.params['login']
        password = request.params['password']
        if USERS.get(login) == password:
            headers = remember(request, login)
            return HTTPFound(location=request.referrer,
                             headers=headers)
        message = 'Failed login'
    return dict(
        page_title="Login",
        message=message,
        url=request.referrer,
        came_from=request.referrer,
        login=login,
        password=password,
        )


@view_config(route_name='home', renderer='templates/home.pt',
                 permission='edit')
def home(request):
    """Prepares the index/home/root page."""
    editor = Editor(data_path = request.registry.settings["editor.data_path"])
    return {"xhtmlFiles":editor.home()}

@view_config(route_name='list', renderer='templates/list.pt',
                 permission='edit')

def list(request):
    """Lists available files."""
    editor = Editor(data_path = request.registry.settings["editor.data_path"])
    return {"xhtmlFiles":editor.list()}


def _getXDoc(basename, request):
    editor = Editor(data_path = request.registry.settings["editor.data_path"])
    if len(basename) > 9 and basename[-9:] == ".html.doc":
        basename = basename[0:-4]
    elif basename[-4:] == ".doc":
        basename = basename[0:-4] + ".html"
    if "version" in request.GET and request.GET["version"]:
        xdoc = editor.select(basename, version = int(request.GET["version"]))
    else:
        xdoc = editor.select(basename)
    return xdoc

def _root_url(request):
    if "editor.root_url" in request.registry.settings:
        return request.registry.settings["editor.root_url"]
    else:
        return request.application_url
    return ret

@view_config(route_name='select', renderer='templates/select.pt',
                 permission='edit')
@view_config(route_name='view_email', renderer='templates/view_email.pt',
                 permission='edit')
@view_config(route_name='view_news', renderer='templates/view_news.pt',
                 permission='edit')
@view_config(route_name='view_web', renderer='templates/view_web.pt',
                 permission='edit')
def select(request):
    """Selects a file and returns the BODY part."""
    xdoc = _getXDoc(request.matchdict["basename"], request)
    ret = xdoc.toDict()
    return ret

@view_config(route_name='filecontent')
def getFileContent(request):
    editor = Editor(data_path = request.registry.settings["editor.data_path"])
    fileList=editor.list();
    fileName=request.matchdict['basename']

    if fileName in fileList :
        xhtmlDoc=editor.select(fileName)
        #returns the body of xhtmlDoc
        return  Response(simplejson.dumps(xhtmlDoc.toDict()))
    else:

        return Response('File doesn\'t exist')

@view_config(route_name='filelist')
def fileList(request):
    listDict=list(request)
    return Response(simplejson.dumps(listDict))
##    fileList=listDict.get("xhtmlFiles")
##    fileList_json=[]
##    i=1
##    for temp in fileList:
##        fileList_json.append((i,temp))
##        i=i+1
##   # print simplejson.dumps(listDict)
##    print fileList_json
##    return Response(simplejson.dumps(dict(fileList_json)))

@view_config(route_name='view_raw', permission='edit')

def selectRaw(request):
    xdoc = _getXDoc(request.matchdict["basename"], request)
    size = os.path.getsize(xdoc.get_path(version = xdoc.version))
    response = Response(content_type='text/html')
    response.app_iter = open(xdoc.get_path(version = xdoc.version), 'rb')
    return response

@view_config(route_name='download', permission='edit')
def download(request):
    xdoc = _getXDoc(request.matchdict["basename"], request)
    size = os.path.getsize(xdoc.get_path(version = xdoc.version))
    response = Response(content_type='application/force-download', content_disposition='attachment; filename=' + request.matchdict["basename"])
    response.app_iter = open(xdoc.get_path(version = xdoc.version), 'rb')
    return response


@view_config(route_name='edit', renderer='templates/edit.pt',
                 permission='edit')
def edit(request):
    """Prepares the edition of a xhtmlFile."""
    xdoc = _getXDoc(request.matchdict["basename"], request)
    xdoc.body = xdoc.body.replace('\n',' ')
    return xdoc.toDict()

@view_config(route_name = 'update', renderer='templates/update.pt',
                 permission='edit')
def update(request):
    """Changes the content of the file."""
    editor = Editor(data_path=request.registry.settings["editor.data_path"])
    return editor.update(request.matchdict["basename"],
                         request.POST["xhtmlBody"],
                         xhtml_title = request.POST["xhtml_title"],
                         layout = request.POST["layout"],
                         rebasename = request.POST["rebasename"]).toDict()

@view_config(route_name ='insert', renderer='templates/insert.pt',
                 permission='edit')
def insert(request):
    """Makes a new XHTML File as basename.xhtml ans with xhtmlBody in the body tag."""
    editor = Editor(data_path=request.registry.settings["editor.data_path"])
    return editor.insert(request.matchdict["basename"],
                         request.POST["xhtmlBody"],
                         xhtml_title = request.POST["xhtml_title"],
                         layout = request.POST["layout"]).toDict()

@view_config(route_name='delete', renderer='templates/delete.pt',
                 permission='edit')
def delete(request):
    """Deletes the basename.xhtml file."""
    editor = Editor(data_path=request.registry.settings["editor.data_path"])
    xdoc = editor.delete(request.matchdict["basename"])
    return xdoc.toDict()

@view_config(route_name ='restore', renderer='templates/update.pt',
                 permission='edit')
def restore(request):
    editor = Editor(data_path=request.registry.settings["editor.data_path"])
    xdoc = editor.restore(request.GET["basename"])
    return xdoc.toDict()

@view_config(route_name='create', renderer='templates/create.pt',
                 permission='edit')
def create(request):
    """Prepares the insert of basename.xhtml (basename to come in post variables)."""
    editor = Editor(data_path=request.registry.settings["editor.data_path"])
    ret = editor.create(request.GET["basename"])
    return ret.toDict()

@view_config(name="logout.html")
def logout(request):
    headers = forget(request)
    return HTTPFound(location=_root_url(request) + "/", headers=headers)